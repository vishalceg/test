package com.genesys.test.util;

import com.genesys.test.users.ActionType;

public class UserUtil {

	public static boolean isValidEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}

	public static String builtUrl(String entity, long entityId, ActionType type) {
		StringBuilder builder = new StringBuilder("http://localhost:6060/api/v1/");
		builder.append(entity).append("/").append(entityId);
		if (type == ActionType.delete) {
			builder.append("/").append(type.name());
		}
		return builder.toString();
	}

}
