package com.genesys.test.users;

public class UserAction {
	private String url;
	private ActionType actionType;
   
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ActionType getType() {
		return actionType;
	}

	public void setType(ActionType actionType) {
		this.actionType = actionType;
	}

	
}
