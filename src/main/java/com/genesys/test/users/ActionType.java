package com.genesys.test.users;

public enum ActionType {
  update, delete
}
