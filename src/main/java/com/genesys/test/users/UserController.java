package com.genesys.test.users;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.genesys.test.exception.CustomerException;
import com.genesys.test.exception.ResourceNotFoundException;
import com.genesys.test.util.UserUtil;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@PostMapping("/users")
	public User create(@RequestBody @Valid UserIn userIn) {
		if (!UserUtil.isValidEmail(userIn.getEmail())) {
			throw new CustomerException("Email is not valid.");
		}
		Optional<User> oldUser = userRepository.findByEmail(userIn.getEmail());
		if (oldUser.isPresent()) {
			throw new CustomerException("Email is already present.");
		}
		// check password strength
		// confirm password
		User user = new User();
		user.setEmail(userIn.getEmail());
		user.setName(userIn.getName());
		user.setPassword(userIn.getPassword());
		return userRepository.save(user);
	}

	@PutMapping("/users/{uid}")
	public User update(@PathVariable long uid, @RequestBody @Valid UserIn userIn) {
		Optional<User> currentUser = userRepository.findById(uid);
		if (!currentUser.isPresent()) {
			throw new ResourceNotFoundException("id-" + uid);
		}
		if (userIn.getEmail() != null && !UserUtil.isValidEmail(userIn.getEmail())) {
			throw new CustomerException("Email is not valid.");
		}
		if (userIn.getEmail() != null && !currentUser.get().getEmail().equals(userIn.getEmail())) {
			Optional<User> oldUser = userRepository.findByEmail(userIn.getEmail());
			if (oldUser.isPresent()) {
				throw new CustomerException("Email is associated with other user..");
			}
			currentUser.get().setEmail(userIn.getEmail());
		}
		// check password strength
		// confirm password
		if (userIn.getName() != null) {
			currentUser.get().setName(userIn.getName());
		}
		if (userIn.getPassword() != null) {
			currentUser.get().setPassword(userIn.getPassword());
		}
		return userRepository.save(currentUser.get());
	}

	@PostMapping("/users/{uid}/delete")
	public User delete(@PathVariable long uid) {
		Optional<User> currentUser = userRepository.findById(uid);
		if (!currentUser.isPresent()) {
			throw new ResourceNotFoundException("id-" + uid);
		}
		userRepository.delete(currentUser.get());
		// to-do deleted=true , as part of response
		return currentUser.get();
	}

	@GetMapping("/users/{uid}/actions")
	public List<UserAction> actions(@PathVariable long uid) {
		Optional<User> currentUser = userRepository.findById(uid);
		if (!currentUser.isPresent()) {
			throw new ResourceNotFoundException("id-" + uid);
		}
		List<UserAction> list = new ArrayList<>();
		for (ActionType type : ActionType.values()) {
			UserAction action = new UserAction();
			action.setType(type);
			action.setUrl(UserUtil.builtUrl("users", currentUser.get().getId(), type));
			list.add(action);
		}
		return list;
	}

	@PostMapping("/users/login")
	public User login(@RequestBody @Valid LoginInput loginInput) {
		Optional<User> currentUser = userRepository.findByEmailAndPassword(loginInput.getEmail(),
				loginInput.getPassword());
		if (!currentUser.isPresent()) {
			throw new CustomerException("Either email or password is wrong.");
		}
		currentUser.get().setLastLogin(new Date());// need to work on date format
		userRepository.save(currentUser.get());
		return currentUser.get();
	}

}
